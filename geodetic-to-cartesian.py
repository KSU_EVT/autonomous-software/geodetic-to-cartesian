import math
import pandas as pd
import argparse

def radius_at_lat_lon(lat, lon):
    lat = math.radians(lat)

    A = 6378137.0 # equatorial radius of WGS ellipsoid, in meters
    B = 6356752.3 # polar radius of WGS ellipsoid, in meters
    r = (A * A * math.cos(lat))**2 + (B * B * math.sin(lat))**2
    r /= (A * math.cos(lat))**2 + (B * math.sin(lat))**2
    r = math.sqrt(r)
    return r

def haversine(lon1, lat1, lon2, lat2, alt):
    lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = math.sin(dlat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon/2)**2
    c = 2 * math.asin(math.sqrt(a))
    r = radius_at_lat_lon((lat1+lat2)/2.0, (lon1+lon2)/2.0)
    r = r + alt
    return c * r

def haversine_bearing(lon1, lat1, lon2, lat2):
    lon1, lat1, lon2, lat2 = map(math.radians, [lon1, lat1, lon2, lat2])

    dLon = (lon2 - lon1)
    y = math.sin(dLon) * math.cos(lat2)
    x = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dLon)
    bearing = math.atan2(y, x)
    bearing = math.degrees(bearing)
    return bearing

def geodetic_to_enu(lat, lon, alt, ref_lat, ref_lon, ref_alt):
    bearing = haversine_bearing(ref_lon, ref_lat, lon, lat)
    distance = haversine(ref_lon, ref_lat, lon, lat, alt - ref_alt)

    E = distance * math.sin(math.radians(bearing))
    N = distance * math.cos(math.radians(bearing))
    U = alt - ref_alt

    return E, N, U

def read_and_convert(csv_path, output_path, origin_cart=None, origin_geo=None):
    df = pd.read_csv(csv_path)
    if 'latitude' in df.columns and 'longitude' in df.columns and 'altitude' in df.columns:
        if origin_geo is not None:
            ref_lat, ref_lon, ref_alt = map(float, origin_geo)
        else:
            ref_lat, ref_lon, ref_alt = df.iloc[0]['latitude'], df.iloc[0]['longitude'], df.iloc[0]['altitude']

        if origin_cart is not None:
            E_origin, N_origin, U_origin = map(float, origin_cart)
        else:
            E_origin = N_origin = U_origin = 0

        ENU_df = convert_to_ENU(df, ref_lat, ref_lon, ref_alt, E_origin, N_origin, U_origin)
        ENU_df.to_csv(output_path, index=False)
    else:
        print("CSV file must contain 'latitude', 'longitude', and 'altitude' columns.")

def convert_to_ENU(df, ref_lat, ref_lon, ref_alt, E_origin, N_origin, U_origin):
    ENU_coords = []

    for index, row in df.iterrows():
        lat, lon, alt = row['latitude'], row['longitude'], row['altitude']
        E, N, U = geodetic_to_enu(lat, lon, alt, ref_lat, ref_lon, ref_alt)

        E = E - E_origin
        N = N - N_origin
        U = U - U_origin

        ENU_coords.append((E, N, U))

    ENU_df = pd.DataFrame(ENU_coords, columns=['E', 'N', 'U'])
    return ENU_df

parser = argparse.ArgumentParser(description='Convert geodetic coordinates to ENU frame.')
parser.add_argument('csvfile', type=str, help='The name of the .csv file with geodetic coordinates')
parser.add_argument('-o', '--output', type=str, default='ENU_coords.csv', help='The name of the output .csv file for ENU coordinates')
parser.add_argument('-c', '--cartesian_origin', type=float, nargs=3, help='Cartesian (E, N, U) origin for ENU frame')
parser.add_argument('-g', '--geodetic_origin', type=float, nargs=3, help='Geodetic (lat, lon, alt) origin for ENU frame')

args = parser.parse_args()
read_and_convert(args.csvfile, args.output, args.cartesian_origin, args.geodetic_origin)
