import pandas as pd
import matplotlib.pyplot as plt
import argparse

def visualize_csv(csv_path):
    # Read data from CSV
    df = pd.read_csv(csv_path)

    # Create a scatter plot of E (East) and N (North) from the dataframe
    plt.scatter(df['E'], df['N'])

    # Optionally plot U (Up) values as color map
    plt.scatter(df['E'], df['N'], c=df['U'], cmap='viridis')

    # Set the labels for the axes
    plt.xlabel('E (East)')
    plt.ylabel('N (North)')

    # Optionally if 'U' values are used
    plt.colorbar(label='U (Up)')

    # Set the title of the plot
    plt.title('ENU Coordinates')

    # Display the plot
    plt.show()

def parse_args():
    parser = argparse.ArgumentParser(
        description="Visualize a CSV file with ENU coordinates.",
        usage="python visualize.py data.csv"
    )
    parser.add_argument(
        "csv_path",
        type=str,
        help="Path to the CSV file containing the ENU coordinates."
    )
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    visualize_csv(args.csv_path)
