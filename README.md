# Geodetic to Cartesian

This repo contains script(s) whihch may convert a .csv of standard WGS84 geodetic lat/lon pairs to a local cartesian reference frame, where (0,0,0) is defined as the first point in the .csv

This may be useful for converting a series of points, such as positions on a racetrack, into a series of 3D points that can be followed by the kart
